provider "aws" {
  region = "us-east-1"
}


resource "aws_instance" "ec2_dev" {
   ami           = "ami-053b0d53c279acc90"
   key_name = aws_key_pair.ssh-key.key_name
   instance_type =  "t2.micro"
   security_groups = [aws_security_group.app_sg.name]
   
   tags = {
           Name = "EC2 Deploy"
   }
}

resource "aws_key_pair" "ssh-key" {
    key_name = "server-key"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDWaUmXPtt3IcdglvcxGsyuGbY6KMWGWs4ewl94+UMua8E6wEpvg/jKualZw4fN71lGrFXIfeECPJwNc/T4k57Owc6SqPzrDDc8OinKQsGnt99i2922HtI09qBk9AJHSp3yvJ0k25bmsngStkuubLuyY/puMTEoqihdxWHrfBTmV5qL6e8xGQGQrYA4CcUrqzWg8o8CbWfUH/8QbB+FFrk3MOnJWch8p7ULNpDnm3Xs5zWRaoaW3gvF0UmiEhPRsrflbYc4rOlc1dBd4OW1aYOufyitjWE9w40XQtXmvHUsUnGhhweNhhKEV1dxL19lhOyZ1V11LsK3NdRwNTMWIja61CdwI9T0JmInTMTLGFTj5Q532ncUPwIZsXoOD+rUgGlEqvZF+VixiCidSxmF1CXVkJTU27zJzNzbYD8ADDaRcszqouTUuBXdTSSyEuWb5hgH1y2FrpxVBsTPawosS6BAjIx8J6jCES98P9eErI2n87JFdycWBOJ39uUWE2zG8Sk= hodat@DESKTOP-QIBE1NQ"
}

output "server_ip" {
    value = aws_instance.ec2_dev.public_ip
}

resource "aws_security_group" "app_sg" {
  name        = "sec_gr"
  description = "Allow SSH inbound traffic"

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] 
  }

  ingress {
    from_port = 3000
    to_port = 3000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0 
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec_gr"
  }
}

terraform {
  backend "s3" {
    bucket         = "my-bucket-hodat"
    key            = "hodat/state/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "table-test"
  }
}
